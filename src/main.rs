use rand::Rng;
use std::time::Duration;
use std::error::Error;
use std::io;
use crossterm::{event::{self, Event, KeyCode, EnableMouseCapture, DisableMouseCapture, poll},
                execute,
                terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen}};
use tui::{backend::{CrosstermBackend}, Terminal, widgets::{Borders, Block}, widgets::canvas::{Canvas, Rectangle}, style::{Color}, layout::{Constraint, Layout}};

// Snake implementation
struct Snake {
    size:  usize,
    head_x: usize,
    head_y: usize,
    body_pos: Vec<usize>,
    direction: Controler
}

impl Snake {
    fn new(size_x: usize, size_y: usize) -> Snake{
        // Limit initial x and y position by the size of the board
        let new_x = rand::thread_rng().gen_range(0..size_x);
        let new_y = rand::thread_rng().gen_range(0..size_y);
        Snake{size: 1, head_x: new_x, head_y: new_y, body_pos: Vec::new(), direction: Controler::Right}
    }

    fn set_direction(&mut self, direction: Controler){
        self.direction = direction;
    }

    fn momentum(&mut self){
        match self.direction{
            Controler::Down => {
                self.head_y -= 1;
            }
            Controler::Up => {
                self.head_y += 1;
            }
            Controler::Left => {
                self.head_x -= 1;
            }
            Controler::Right => {
                self.head_x += 1;
            }
            Controler::Exit => {
                std::process::exit(0);
            }
        }
    }
}

//Controler implementation
#[derive(Clone, Copy)]
enum Controler {
    Up,
    Down,
    Left,
    Right,
    Exit,
}

//Food implementation
struct Food{
    pos_x: usize,
    pos_y: usize,
    max_x: usize,
    max_y: usize
}

impl Food {
    fn new(max_x: usize, max_y: usize) -> Food{
        // Limit initial x and y position by the size of the board
        let new_x = rand::thread_rng().gen_range(0..max_x);
        let new_y = rand::thread_rng().gen_range(0..max_y);
        Food{pos_x: new_x, pos_y: new_y, max_x: max_x, max_y: max_y}
    }

    fn respawn(&mut self) {
        // Limit initial x and y position by the size of the board
        let new_x = rand::thread_rng().gen_range(0..self.max_x);
        let new_y = rand::thread_rng().gen_range(0..self.max_y);

        //Set new x and y positions
        self.pos_x = new_x;
        self.pos_y = new_y;
    }

}

//Game implementation
struct Game {
    running: bool,
    score: usize,
    snake: Snake,
    speed: u64
}

impl Game{
    fn new(snake: Snake, speed: u64) -> Game{
        Game{running: false, score: 0, snake: snake, speed: speed}
    }

    fn read_key(&self) -> crossterm::Result<Controler>{
        if poll(Duration::from_millis(self.speed))?{ //This removes the block from key read
        let key = if let Event::Key(key) = event::read()? {key} else {todo!()};
            match key.code {
                KeyCode::Char('q') => {
                    return Ok(Controler::Exit);
                }
                KeyCode::Down => {
                    return Ok(Controler::Down);
                }
                KeyCode::Up => {
                    return Ok(Controler::Up);
                }
                KeyCode::Right => {
                    return Ok(Controler::Right);
                }
                KeyCode::Left => {
                    return Ok(Controler::Left);
                }
                _ => Ok(self.snake.direction.clone())
            }
        }
        else{
            Ok(self.snake.direction.clone())
        }
    }

    fn run_game(&mut self, term: &mut Terminal<CrosstermBackend<io::Stdout>>) -> crossterm::Result<()>{
        loop{
            //TODO: Make snake body
            //TODO: Deal with borders
            //TODO: Draw food
            //TODO: Catch collisions
            //TODO: Make snake grow
            //TODO: Score related features

            self.snake.momentum(); //Move snake in each tick

            let input_key = self.read_key()?;
            self.snake.set_direction(input_key); //Key read sets direction in next tick

            term.draw(|f|{
                let chunks = Layout::default()
                    .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
                    .split(f.size());
                let canvas = Canvas::default()
                    .block(Block::default().title("snakers").borders(Borders::ALL))
                    .x_bounds([0.0, 50.0])
                    .y_bounds([0.0, 50.0])
                    .paint(|ctx| {
                        //ctx.layer();
                        ctx.draw(&Rectangle {
                            x: self.snake.head_x as f64,
                            y: self.snake.head_y as f64,
                            width: 2.0, //Hardcoded snake size
                            height: 2.0,
                            color: Color::Red
                        });
                    });
                f.render_widget(canvas, chunks[0]);
            })?;
        }
    }
}


fn main() -> Result<(), Box<dyn Error>>{
    //Tui black magic taken from https://github.com/fdehau/tui-rs/blob/master/examples/user_input.rs
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    //Set canvas size
    let (size_x, size_y) = (15, 15);

    let snake: Snake = Snake::new(size_x, size_y);
    let mut game: Game = Game::new(snake, 100u64);
    game.run_game(&mut terminal)?;

    //Restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;
    Ok(())
}
